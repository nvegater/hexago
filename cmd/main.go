package main

import (
	"hexaGoApi/internal/adapters/left/grpc"
	"hexaGoApi/internal/adapters/right/database"
	"hexaGoApi/internal/application"
	"hexaGoApi/internal/application/core/arithmetic"
	"log"
	"os"
)

func main() {

	var err error

	// Data source name = Connection String of the database
	databaseDriver := os.Getenv("DB_DRIVER")
	dataSourceName := os.Getenv("DB_DSNAME")
	db, error := database.New(databaseDriver, dataSourceName)

	if error != nil {
		log.Fatalf("failed to initiate database connection: %v", err)
	}
	// close connection when the func return
	defer db.CloseDbConnection()

	// core
	core := arithmetic.New()

	app := application.New(core, db)

	grpc := rpc.New(app)

	grpc.Run()
}
