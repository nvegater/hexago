FROM golang:1.18.3-alpine3.16

EXPOSE 9000

RUN apk update \
    && apk add --no-cache \
        mysql-client \
        build-base

RUN mkdir "/app"
WORKDIR /app

# Copy host file to container
COPY go.mod .
COPY go.sum .

# Download dependencies from go mod file
RUN go mod download

# Copy all from host working dir to container
COPY . .

# Script to wait for the MYSQL container to start
COPY ./grpc_entrypoint.sh /usr/local/bin/grpc_entrypoint.sh

# Change permissions of file to make it executable.
RUN /bin/chmod +x /usr/local/bin/grpc_entrypoint.sh

# Create a binary called main
RUN go build cmd/main.go

# Move binary
RUN mv main /usr/local/bin/

# Start primary process from container (run our app)
CMD ["main"]

ENTRYPOINT ["grpc_entrypoint.sh"]
