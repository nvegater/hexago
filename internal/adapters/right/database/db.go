package database

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

type Adapter struct {
	db *sql.DB
}

func New(driverName, dataSourceName string) (*Adapter, error) {
	// connect
	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		log.Fatalf("DB Connection failure:  %v ", err)
	}
	// test database connection
	err = db.Ping()
	if err != nil {
		log.Fatalf("DB Ping failure:  %v ", err)
	}
	return &Adapter{db: db}, nil
}

func (dbAdapter Adapter) CloseDbConnection() {
	err := dbAdapter.db.Close()
	if err != nil {
		log.Fatalf("DB Closing failure, %v", err)
	}
}

func (dbAdapter Adapter) AddToHistory(answer int32, operation string) (int64, error) {
	queryString, args, err := sq.
		Insert("arithmetic_history").Columns("date", "answer", "operation").
		Values(time.Now(), answer, operation).
		ToSql()

	if err != nil {
		return 0, err
	}

	res, err := dbAdapter.db.Exec(queryString, args...)

	if err != nil {
		return 0, err
	}

	noOfAffectedRows, err := res.RowsAffected()

	if err != nil {
		return 0, err
	}

	return noOfAffectedRows, nil

}
