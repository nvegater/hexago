package rpc

import (
	"google.golang.org/grpc"
	"hexaGoApi/internal/adapters/left/grpc/generatedProtoLib"
	"hexaGoApi/internal/ports"
	"log"
	"net"
)

type Adapter struct {
	api ports.APIPort
}

func New(api ports.APIPort) *Adapter {
	return &Adapter{api: api}
}

func (grpcAdapter Adapter) Run() {
	var err error

	listen, err := net.Listen("tcp", ":9000")

	if err != nil {
		log.Fatalf("failed to listen on port 9000: %v", err)
	}

	arithmeticServiceServer := grpcAdapter
	grpcServer := grpc.NewServer()

	generatedProtoLib.RegisterArithmeticServiceServer(grpcServer, arithmeticServiceServer)

	if err := grpcServer.Serve(listen); err != nil {
		log.Fatalf("failed to serve Grpc on port 9000: %v", err)

	}
}
