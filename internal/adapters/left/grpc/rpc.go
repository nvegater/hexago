package rpc

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"hexaGoApi/internal/adapters/left/grpc/generatedProtoLib"
)

func (grpcAdapter Adapter) GetAddition(
	ctx context.Context,
	req *generatedProtoLib.OperationParameters) (*generatedProtoLib.Answer, error) {
	var err error
	answer := &generatedProtoLib.Answer{}
	if req.GetA() == 0 || req.GetB() == 0 {
		return answer, status.Error(codes.InvalidArgument, "missing required")
	}

	operationResult, err := grpcAdapter.api.GetAddition(req.A, req.B)

	if err != nil {
		return answer, status.Error(codes.Internal, "unexpected error")
	}

	answer = &generatedProtoLib.Answer{Value: operationResult}

	return answer, nil

}
func (grpcAdapter Adapter) GetSubtraction(
	ctx context.Context,
	req *generatedProtoLib.OperationParameters) (*generatedProtoLib.Answer, error) {
	var err error
	answer := &generatedProtoLib.Answer{}
	if req.GetA() == 0 || req.GetB() == 0 {
		return answer, status.Error(codes.InvalidArgument, "missing required")
	}

	operationResult, err := grpcAdapter.api.GetSubtraction(req.A, req.B)

	if err != nil {
		return answer, status.Error(codes.Internal, "unexpected error")
	}

	answer = &generatedProtoLib.Answer{Value: operationResult}

	return answer, nil

}
func (grpcAdapter Adapter) GetMultiplication(
	ctx context.Context,
	req *generatedProtoLib.OperationParameters) (*generatedProtoLib.Answer, error) {
	var err error
	answer := &generatedProtoLib.Answer{}
	if req.GetA() == 0 || req.GetB() == 0 {
		return answer, status.Error(codes.InvalidArgument, "missing required")
	}

	operationResult, err := grpcAdapter.api.GetMultiplication(req.A, req.B)

	if err != nil {
		return answer, status.Error(codes.Internal, "unexpected error")
	}

	answer = &generatedProtoLib.Answer{Value: operationResult}

	return answer, nil

}
func (grpcAdapter Adapter) GetDivision(
	ctx context.Context,
	req *generatedProtoLib.OperationParameters) (*generatedProtoLib.Answer, error) {
	var err error
	answer := &generatedProtoLib.Answer{}
	if req.GetA() == 0 || req.GetB() == 0 {
		return answer, status.Error(codes.InvalidArgument, "missing required")
	}

	operationResult, err := grpcAdapter.api.GetDivision(req.A, req.B)

	if err != nil {
		return answer, status.Error(codes.Internal, "unexpected error")
	}

	answer = &generatedProtoLib.Answer{Value: operationResult}

	return answer, nil

}
