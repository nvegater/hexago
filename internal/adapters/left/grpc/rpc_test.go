package rpc

import (
	"context"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
	"hexaGoApi/internal/adapters/left/grpc/generatedProtoLib"
	"hexaGoApi/internal/adapters/right/database"
	"hexaGoApi/internal/application"
	"hexaGoApi/internal/application/core/arithmetic"
	"log"
	"net"
	"os"
	"testing"
)

// buffcon is native to Golang.
// it starts a server but not on a real socket or port.
// the client can nevertheless interact with it, as if it was a real server.
// all happens in memory but is the same expected behaviour of a normal network connection.

const bufSize = 1024 * 1024

var listener *bufconn.Listener

// init will run before all the tests
func init() {
	var err error

	listener = bufconn.Listen(bufSize)

	grpcServer := grpc.NewServer()

	// Data source name = Connection String of the database
	databaseDriver := os.Getenv("DB_DRIVER")
	dataSourceName := os.Getenv("DB_DSNAME")
	db, error := database.New(databaseDriver, dataSourceName)

	if error != nil {
		log.Fatalf("failed to initiate database connection: %v", err)
	}

	// core
	core := arithmetic.New()

	app := application.New(core, db)

	grpc := New(app)

	generatedProtoLib.RegisterArithmeticServiceServer(grpcServer, grpc)

	go func() {
		if err := grpcServer.Serve(listener); err != nil {
			log.Fatalf("test server start error, %v", err)
		}
	}()

}

func getGRPCConnection(ctx context.Context, t *testing.T) *grpc.ClientConn {
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufferDialer), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error dialing bufnet: %v", err)
	}
	return conn
}

// bufferDialer creates local-memory connection that emulates a network connection
func bufferDialer(context.Context, string) (net.Conn, error) {
	return listener.Dial()
}

func TestGetAddition(t *testing.T) {
	ctx := context.Background() // EMPTY Context but we need it for the GRPC connection

	conn := getGRPCConnection(ctx, t)
	// close the connection at the end of the function
	defer conn.Close()

	// Create client to talk with the server.
	client := generatedProtoLib.NewArithmeticServiceClient(conn)

	params := &generatedProtoLib.OperationParameters{A: 1, B: 1}

	answer, err := client.GetAddition(ctx, params)

	if err != nil {
		t.Fatalf("expected: %v, got %v", nil, err)
	}
	require.Equal(t, answer.Value, int32(2))

}
func TestGetSubtraction(t *testing.T) {
	ctx := context.Background() // EMPTY Context but we need it for the GRPC connection

	conn := getGRPCConnection(ctx, t)
	// close the connection at the end of the function
	defer conn.Close()

	// Create client to talk with the server.
	client := generatedProtoLib.NewArithmeticServiceClient(conn)

	params := &generatedProtoLib.OperationParameters{A: 1, B: 1}

	answer, err := client.GetSubtraction(ctx, params)

	if err != nil {
		t.Fatalf("expected: %v, got %v", nil, err)
	}
	require.Equal(t, answer.Value, int32(0))

}
func TestGetMultiplication(t *testing.T) {
	ctx := context.Background() // EMPTY Context but we need it for the GRPC connection

	conn := getGRPCConnection(ctx, t)
	// close the connection at the end of the function
	defer conn.Close()

	// Create client to talk with the server.
	client := generatedProtoLib.NewArithmeticServiceClient(conn)

	params := &generatedProtoLib.OperationParameters{A: 1, B: 1}

	answer, err := client.GetMultiplication(ctx, params)

	if err != nil {
		t.Fatalf("expected: %v, got %v", nil, err)
	}
	require.Equal(t, answer.Value, int32(1))

}
func TestGetDivision(t *testing.T) {
	ctx := context.Background() // EMPTY Context but we need it for the GRPC connection

	conn := getGRPCConnection(ctx, t)
	// close the connection at the end of the function
	defer conn.Close()

	// Create client to talk with the server.
	client := generatedProtoLib.NewArithmeticServiceClient(conn)

	params := &generatedProtoLib.OperationParameters{A: 1, B: 1}

	answer, err := client.GetDivision(ctx, params)

	if err != nil {
		t.Fatalf("expected: %v, got %v", nil, err)
	}
	require.Equal(t, answer.Value, int32(1))

}
