package arithmetic

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestArithmeticAdapter_Addition(t *testing.T) {
	arith := New()
	answer, err := arith.Addition(1, 1)

	if err != nil {
		t.Fatalf("expected nil and got an error: %v", err)
	}

	require.Equal(t, answer, int32(2))
}

func TestArithmeticAdapter_Subtraction(t *testing.T) {
	arith := New()
	answer, err := arith.Subtraction(1, 1)

	if err != nil {
		t.Fatalf("expected nil and got an error: %v", err)
	}

	require.Equal(t, answer, int32(0))
}

func TestArithmeticAdapter_Multiplication(t *testing.T) {
	arith := New()
	answer, err := arith.Multiplication(1, 1)

	if err != nil {
		t.Fatalf("expected nil and got an error: %v", err)
	}

	require.Equal(t, answer, int32(1))
}

func TestArithmeticAdapter_Division(t *testing.T) {
	arith := New()
	answer, err := arith.Division(1, 1)

	if err != nil {
		t.Fatalf("expected nil and got an error: %v", err)
	}

	require.Equal(t, answer, int32(1))
}
