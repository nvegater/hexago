package application

import (
	"hexaGoApi/internal/ports"
)

type StructApp struct {
	arithmeticPort Arithmetic
	dbPort         ports.DbPort
}

func New(arithmetic Arithmetic, db ports.DbPort) *StructApp {
	return &StructApp{arithmeticPort: arithmetic, dbPort: db}
}

func (ingredients StructApp) GetAddition(a int32, b int32) (int32, error) {

	result, err := ingredients.arithmeticPort.Addition(a, b)

	if err != nil {
		return 0, err
	}

	_, err = ingredients.dbPort.AddToHistory(result, "addition")

	if err != nil {
		return 0, err
	}

	return result, nil
}
func (ingredients StructApp) GetSubtraction(a int32, b int32) (int32, error) {
	result, err := ingredients.arithmeticPort.Subtraction(a, b)

	if err != nil {
		return 0, err
	}

	_, err = ingredients.dbPort.AddToHistory(result, "substraction")

	if err != nil {
		return 0, err
	}

	return result, nil
}
func (ingredients StructApp) GetMultiplication(a int32, b int32) (int32, error) {
	result, err := ingredients.arithmeticPort.Multiplication(a, b)

	if err != nil {
		return 0, err
	}

	_, err = ingredients.dbPort.AddToHistory(result, "multiplication")

	if err != nil {
		return 0, err
	}

	return result, nil
}
func (ingredients StructApp) GetDivision(a int32, b int32) (int32, error) {
	result, err := ingredients.arithmeticPort.Division(a, b)

	if err != nil {
		return 0, err
	}

	_, err = ingredients.dbPort.AddToHistory(result, "division")

	if err != nil {
		return 0, err
	}

	return result, nil
}
