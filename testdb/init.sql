CREATE TABLE arithmetic_history (
    date DATE NOT NULL,
    answer INT NOT NULL,
    operation VARCHAR(255) NOT NULL
)