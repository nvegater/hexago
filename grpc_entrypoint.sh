#!/bin/sh
# This scripts just waits and tries connections until MySQL Starts
# REASON:
# App container starts immediately
# MySQL Container takes longer to start
# App container crashes if MySQL is not ready

set -e # immediately exit script on error
COMMAND=$@ # command that get passed to the script. Should be in the docker file

echo 'Waiting for database to be available...'
maxTries=20
while [ "$maxTries" -gt 0 ] && ! mysql -h "$MYSQL_HOST" -P "$MYSQL_PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DB" -e 'SHOW TABLES'; do
    maxTries=$(($maxTries - 1))
    sleep 3 # wait 3 secs to try again
done
echo
if [ "$maxTries" -le 0 ]; then
    echo >&2 'error: unable to contact mysql after 10 tries'
    exit 1
fi

exec $COMMAND